package org.fast.search.hash;

/**
 * String search table that use hash code based on multiplying.
 * @author Nenad.Vico
 */
public class StringSearchTable extends SearchTable<String>
{
    private int number = 11;

    /**
     * Constructor.
     * @param number number for multiplying
     */
    public StringSearchTable(int number) {
        this.number = number;
    }

    /**
     * Calculate hash code for string based on multiplying.
     * @param object object to calculate.
     * @return Returns hash code.
     */
    protected int hashCode(String object) {
        int h = 0;
        int len = object.length();
        if (len > 0) {
            char[] val = new char[len];
            object.getChars(0, len, val, 0);
            for (int i = 0; i < len; i++) {
                h = number*h + val[i];
            }
        }
        return h;
    }

}
