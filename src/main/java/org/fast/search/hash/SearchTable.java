package org.fast.search.hash;

import java.util.HashSet;
import java.util.Set;

/**
 * Used for fast memory search based on hash codes. Object requires 512 MB of memory.
 * @param <T> object type.
 * @author Nenad.Vico
 */

public class SearchTable<T>
{
    private HashTable hashTable = new HashTable();
    private Set<T> set = new HashSet<T>();

    /**
     * Adds object to hashTable.
     * @param object object to add.
     * @return Returns true if already exists, otherwise false.
     */
    public final boolean add(T object) {
        boolean result = hashTable.add(hashCode(object));
        if (result) {
            set.add(object);
        }
        return result;
    }

    /**
     * Removes object from hashTable.
     * @param object object to remove.
     * @return Returns true if removed, otherwise false.
     */
    public final boolean remove(T object) {
        int hash = hashCode(object);
        boolean result = hashTable.contains(hash);
        if (result) {
            if (!set.remove(object)) {
                hashTable.remove(hash);
            }
        }
        return result;
    }

    /**
     * Determines if object contains in hashTable.
     * True result is not curtain, because search is based on hash codes.
     * @param object object to check.
     * @return Returns true if object contains in hashTable.
     */
    public final boolean contains(T object) {
        return hashTable.contains(hashCode(object));
    }

    /**
     * Calculate hash code for object.
     * @param object object to calculate.
     * @return Returns hash code.
     */
    protected int hashCode(T object) {
        return object.hashCode();
    }

}
